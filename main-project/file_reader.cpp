#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>



date convert(const char* str)
{
    date result;
    char buffer[20];
    strcpy(buffer, str);
    char* context = NULL;
    char* str_number = strtok_s(buffer, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    return result;
}

void read(const char* file_name, Precipitation* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (file >> tmp_buffer)  // read start date
        {
            Precipitation* item = new Precipitation;
            item->start = convert(tmp_buffer);

            file >> tmp_buffer; // read end date
            item->end = convert(tmp_buffer);

            file >> item->amount; // read amount

            file.ignore(1, ' '); // ignore the space before type

            file.getline(item->type, MAX_STRING_SIZE); // read type

            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}