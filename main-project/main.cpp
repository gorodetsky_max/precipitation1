#include <iostream>
#include <iomanip>
#include <locale>
#include "file_reader.h"
#include "constants.h"

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Laboratory work #4. GIT\n";
    cout << "Variant #3. Precipitation\n";
    cout << "Author: Maxim Gorodetsky\n\n";
    cout << "Group: 23PI1d\n";

    Precipitation* precipitations[MAX_FILE_ROWS_COUNT];
    int size;

    try
    {
        read("data.txt", precipitations, size);
        cout << "***** ������ *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ������ **********/
            cout << "���� ������.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.day << '\n';
            cout << "����� ������....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.month << '\n';
            /********** ����� ����� **********/
            cout << "���� �����......: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.day << '\n';
            cout << "����� �����.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.month << '\n';
            /********** ����� ���������� � ���� **********/
            cout << "����������......: " << fixed << setprecision(2) << precipitations[i]->amount << '\n';
            cout << "���.............: " << precipitations[i]->type << '\n';
            cout << '\n';
        }

        for (int i = 0; i < size; i++)
        {
            delete precipitations[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    return 0;
}
