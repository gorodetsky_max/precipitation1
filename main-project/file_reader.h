#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "precipitation1.h"

void read(const char* file_name, Precipitation* array[], int& size);

#endif